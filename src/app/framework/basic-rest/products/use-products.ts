import { useMutation, useQueryClient } from "react-query";
import { API_ENDPOINTS } from "../../utils/api.endpoints";
import http from "../../utils/http";
import { fetchProduct } from "./get-products";

export interface ProductInputType {
    id?: number | undefined;
    name: string;
    description: string;
}

async function store(input: ProductInputType) {
    const product = await http.post(API_ENDPOINTS.PRODUCTS, input)
    return product;
}

export const useStoreProductMutation = () => {
    const queryClient = useQueryClient()
    return useMutation((input: ProductInputType) => store(input), {
        onSuccess: (data) => {
            queryClient.prefetchQuery([API_ENDPOINTS.PRODUCTS], () => fetchProduct());
        },
        onError: (data) => {
            console.log(data, 'error response');
        },
    });
};

async function deleteProduct(id: number){
    return await http.delete(`${API_ENDPOINTS.PRODUCTS}` + '/' + id)
}

export const useDeleteProductMutation = () => {
    const queryClient = useQueryClient()
    return useMutation((id: number) => deleteProduct(id), {
        onSuccess: (data) => {
            queryClient.prefetchQuery([API_ENDPOINTS.PRODUCTS], () => fetchProduct());
        },
        onError: (data) => {
            console.log(data, 'ChangeEmail error response');
        },
    });
};