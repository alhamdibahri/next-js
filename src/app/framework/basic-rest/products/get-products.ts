import { Product } from '../../types';
import http from '../../utils/http';
import { API_ENDPOINTS } from '../../utils/api.endpoints';
import { useQuery } from 'react-query';

export const fetchProduct = async () => {
  const { data } = await http.get(`${API_ENDPOINTS.PRODUCTS}`);
  return data;
};
export const useProductQuery = () => {
  return useQuery<Product, Error>([API_ENDPOINTS.PRODUCTS], () =>
    fetchProduct()
  );
};
