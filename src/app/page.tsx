'use client';
import Image from 'next/image'
import { Inter } from 'next/font/google'
import { useProductQuery } from './framework/basic-rest/products/get-products';
import { ProductInputType, useDeleteProductMutation, useStoreProductMutation } from './framework/basic-rest/products/use-products';
import { useState } from 'react';

const inter = Inter({ subsets: ['latin'] })

export default function Home() {

  const [dataForm, setDataForm] = useState<ProductInputType>({
    name: '',
    description: ''
  })

  const [index, setIndex] = useState<number>(-1)

  const { data, isLoading } = useProductQuery();
  const { mutate: deleteProduct } = useDeleteProductMutation();
  const { mutate: store } = useStoreProductMutation();

  const deletehandler = async (deleteId: number) => {
    await deleteProduct(deleteId);
    alert('data berhasil di hapus')
  }

  const editHandler = async (item: ProductInputType, id: number) => {
    console.log(item)
    setIndex(id)
    setDataForm({
      ...dataForm,
      name: item.name,
      description: item.description
    })
  }

  const clearHandler = () => {
    setIndex(-1)
    setDataForm({
      ...dataForm,
      name: '',
      description: ''
    })
  }

  const saveHandler = async () => {
    if (index === -1) {
      await store(dataForm);
    } else {
      await store({ ...dataForm, id: index });
    }
    alert('data berhasil disimpan')
    clearHandler()
  }

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">

    <div style={{ width: '100%' }}>
      <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Product Name
          </label>
          <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" placeholder="Product Name" defaultValue={dataForm.name} onChange={(e: any) => {
            setDataForm({
              ...dataForm,
              name: e.target.value
            })
          }} />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Product Description
          </label>
          <textarea className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Product Desription" defaultValue={dataForm.description} onChange={(e: any) => {
            setDataForm({
              ...dataForm,
              description: e.target.value
            })
          }} />
        </div>
        <div className="flex items-center justify-between">
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button" onClick={() => saveHandler()} >
            Save
          </button>
          <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button" onClick={() => clearHandler()} >
            Clear
          </button>
        </div>
      </form>
    </div>
    <div style={{ width: '100%' }}>
      {data?.length > 0 ? (
        data?.map((item: any, index: any) => (
          <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="px-6 py-4">
              <div className="font-bold text-xl mb-2 text-center">{item.name}</div>
              <p className="text-gray-700 text-base">
                {item.description}
              </p>
              <div className='mt-5'>
                <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" onClick={() => editHandler(item, item.id)}>
                  Edit
                </button>
                |
                <button className="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded" onClick={() => deletehandler(item.id)}>
                  Hapus
                </button>
              </div>
            </div>
          </div>
        ))
      ) : (
        <div className="border-2 border-border-base rounded font-semibold p-5 px-10 text-brand-danger flex justify-start items-center min-h-[112px] h-full">
          Data Belum Ada
        </div>
      )}
    </div>
    </main>
  )
}
